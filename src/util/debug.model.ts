export interface DebugEvent {
  fps: number;
  currentAngle: number;
  targetAngle: number;
  normalizedDistance: number;
}
