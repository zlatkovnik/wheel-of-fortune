import { WheelConfig } from './wheel.model';

export class WheelService {
  static getWheelConfig() {
    return new Promise<WheelConfig>((resolve, reject) => {
      setTimeout(() => {
        const config = this.dummyConfig();
        resolve(config);
        // reject('Server error!');
      }, 200);
    });
  }

  static dummyConfig(): WheelConfig {
    return {
      animationDuration: 10000,
      segments: [
        {
          label: '10',
          color: '#e53935',
        },
        {
          label: '20',
          color: '#fb8c00',
        },
        {
          label: '50',
          color: '#00acc2',
        },
        {
          label: '100',
          color: '#7db343',
        },
        {
          label: '200',
          color: '#e53935',
        },
        {
          label: '500',
          color: '#fb8c00',
        },
        {
          label: '1000',
          color: '#00acc2',
        },
        {
          label: '10000',
          color: '#7db343',
        },
      ],
    };
  }
}
