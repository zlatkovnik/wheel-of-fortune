export interface WheelConfig {
  animationDuration: number;
  segments: {
    label: string;
    color: string;
  }[];
}
