import './wheel.style.css';
import { DebugEvent } from '../util/debug.model';
import { Easing } from '../util/easing.service';
import { WheelService } from './wheel.service';
import { WheelConfig } from './wheel.model';

export class Wheel {
  // HTML
  private element: HTMLElement;
  // DEBUG
  debugMode = false;
  private debugObservers: Array<(debugEvent: DebugEvent) => void> = [];
  // WHEEL
  private config: WheelConfig;
  private easingFunction: (x: number) => number;
  private cooroutineId: number = 0;
  private lastFrameTime = 0;
  private keyFrameTime = 0;
  private startingAngle = 0;
  private currentAngle = 0;
  private targetAngle = 0;
  private wheelSound: HTMLAudioElement;

  constructor(easingFunction = Easing.easeOutBack) {
    this.easingFunction = easingFunction;
    this.wheelSound = new Audio(require('./wheel_spin.mp3'));
  }

  async draw(parent: HTMLElement) {
    this.element = document.createElement('div');
    this.element.classList.add('loading');
    this.element.innerHTML = 'Loading...';
    parent.appendChild(this.element);
    try {
      this.config = await WheelService.getWheelConfig();
      this.element.classList.replace('loading', 'wheel');
      this.element.innerHTML = '';
      this.drawSegments();
      this.drawLabels();
      this.element.onclick = () => {
        this.handleClick();
      };
    } catch (err) {
      this.element.classList.add('error');
      this.element.innerHTML = err;
    }
  }

  private drawSegments() {
    let cumAngle = -Math.PI / 2;
    const deltaAngle = (2 * Math.PI) / this.config.segments.length;
    this.config.segments.forEach((seg) => {
      const segmentEl = document.createElement('div');
      segmentEl.classList.add('segment');
      const x1 = Math.cos(cumAngle) + 0.5;
      const y1 = Math.sin(cumAngle) + 0.5;
      const x2 = Math.cos(cumAngle + deltaAngle) + 0.5;
      const y2 = Math.sin(cumAngle + deltaAngle) + 0.5;
      segmentEl.style.clipPath = `polygon(${100 * x1}% ${100 * y1}%, 50% 50%, ${100 * x2}% ${100 * y2}%)`;
      segmentEl.style.backgroundColor = seg.color;
      this.element.appendChild(segmentEl);
      cumAngle += deltaAngle;
    });
  }

  private drawLabels() {
    let cumAngle = 0;
    const deltaAngle = (2 * Math.PI) / this.config.segments.length;
    this.config.segments.forEach((seg) => {
      const labelEl = document.createElement('div');
      this.element.appendChild(labelEl);
      labelEl.classList.add('label');
      labelEl.innerHTML = seg.label;
      const t1 = 'translate(-50%, -50%) ';
      const t2 = `rotate(${cumAngle + deltaAngle / 2}rad) `;
      const t3 = `translate(0px, -${40}vw)`;
      labelEl.style.transform = t1 + t2 + t3;
      labelEl.style.fontSize = `${8}vw`;
      cumAngle += deltaAngle;
    });
  }

  private async handleClick() {
    if (this.cooroutineId > 0) {
      return;
    }

    this.cooroutineId = requestAnimationFrame((currentFrameTime) => {
      this.targetAngle = Math.random() * 500 + 1000;
      this.startingAngle = this.currentAngle;
      this.lastFrameTime = currentFrameTime;
      this.keyFrameTime = 0;

      const playbackRate = (this.wheelSound.duration * 1000) / this.config.animationDuration;
      this.wheelSound.playbackRate = playbackRate;
      this.wheelSound.play();
      this.update(currentFrameTime);
    });
  }

  private update(currentFrameTime: number) {
    const delta = currentFrameTime - this.lastFrameTime;
    this.lastFrameTime = currentFrameTime;

    const normalizedTime = this.normalizeTime();
    const x = this.easingFunction(normalizedTime);
    let angleTraveled = this.denormalizeAngle(x);
    if (this.keyFrameTime >= this.config.animationDuration) {
      angleTraveled = this.denormalizeAngle(this.easingFunction(1));
      cancelAnimationFrame(this.cooroutineId);
      this.cooroutineId = 0;
    }
    this.currentAngle = angleTraveled + this.startingAngle;
    this.keyFrameTime += delta;
    this.element.style.transform = `rotate(${this.currentAngle}deg)`;

    if (this.debugMode) {
      const debugEvent: DebugEvent = {
        fps: Math.floor(1000 / delta),
        currentAngle: angleTraveled,
        targetAngle: this.targetAngle,
        normalizedDistance: x,
      };
      this.debugObservers.forEach((o) => o(debugEvent));
    }
    if (this.cooroutineId) {
      requestAnimationFrame((e) => this.update(e));
    }
  }

  private normalizeTime() {
    return this.keyFrameTime / this.config.animationDuration;
  }

  private denormalizeAngle(x: number) {
    return x * this.targetAngle;
  }

  subscribeToFramerateEvent(observer: (event: DebugEvent) => void) {
    this.debugObservers.push(observer);
  }
}
