import './style.css';
import { DebugEvent } from './util/debug.model';
import { Easing } from './util/easing.service';
import { Wheel } from './wheel/wheel.component';

function main() {
  const urlParams = new URLSearchParams(window.location.search);
  const debugMode = !!urlParams.get('debug');

  const wheel = new Wheel(Easing.easeOutQuart);
  wheel.debugMode = debugMode;
  const container = document.createElement('div');
  container.classList.add('container');
  document.body.appendChild(container);
  wheel.draw(container);

  // DEBUG
  if (debugMode) {
    handleDebug();
  }

  function handleDebug() {
    const debugContainer = document.createElement('div');
    debugContainer.classList.add('debug');
    container.appendChild(debugContainer);
    const fpsElement = document.createElement('div');
    debugContainer.appendChild(fpsElement);
    const angleDiffEl = document.createElement('div');
    debugContainer.appendChild(angleDiffEl);
    const normalizedDistEl = document.createElement('div');
    debugContainer.appendChild(normalizedDistEl);
    wheel.subscribeToFramerateEvent((debugEvent: DebugEvent) => {
      fpsElement.innerHTML = `fps: ${debugEvent.fps.toString()}`;
      angleDiffEl.innerHTML = `diff: ${(debugEvent.targetAngle - debugEvent.currentAngle).toFixed(2).toString()} deg`;
      normalizedDistEl.innerHTML = `nDist: ${debugEvent.normalizedDistance.toFixed(2).toString()}`;
    });
  }
}

main();
